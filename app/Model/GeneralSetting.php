<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GeneralSetting extends Model
{
    protected $table='general_setting';
    protected $fillable = [
        'app_name', 'phone_number_1', 'phone_number_2', 'e_mail','app_logo'
    ];
}
