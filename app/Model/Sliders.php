<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
    protected $fillable = [
        'image', 'sliders_category', 'sliders_tag', 'sliders_title'
    ];

}
