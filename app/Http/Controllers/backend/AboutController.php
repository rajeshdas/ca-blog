<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Model\Sliders;
use Illuminate\Http\Request;
use Redirect;
use DB;

class AboutController extends Controller
{
    public function dashboard_about()
    {
        $slidersCount = Sliders::all()->count();
        return view('backend.about.index');
    }
    // public function dashboard_about()
    // {
    //     $slidersCount = DB::table('sliders')->get();
    //     $appCount = DB::table('general_setting')->get();
    //     return view('frontend.about', compact('slidersCount','appCount'));

    // }
    public function about_create()
    {
        return view('backend.about.create');
    }
    public function dashboard_about_store(Request $request)
    {
        $data=array();
        
    	$data['sliders_category']=$request->sliders_category;
    	$data['sliders_title']=$request->sliders_title;
        $data['sliders_name']=$request->sliders_name;
    	$data['sliders_tag']=$request->sliders_tag;

        $image=$request->file('sliders_image');
    	if($image)
      {
			$image_name=str_random(20);
			$ext=strtolower($image->getClientOriginalExtension());
			$image_full_name=$image_name.'.'.$ext;
			$upload_path='image/';
			$image_url=$upload_path.$image_full_name;
			$success=$image->move($upload_path,$image_full_name);
			if($success)
             {
				$data['sliders_image']=$image_url;
			 }
           DB::table('sliders')->insert($data);
           \Session::flash('flash_message','successfully saved.');
           
    	}
        return view('backend.home.create');
    }
    public function deletesliders($sliders_id){
    	DB::table('sliders')
    	->where('sliders_id',$sliders_id)
    	->delete();
        
        Session::flash('flash_message','successfully deleted.');
    	return Redirect()->back();        
    }
    public function showesliders($sliders_id){

    }
}
