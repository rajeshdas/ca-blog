<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Redirect;
use Session;

class PartnersController extends Controller
{
    public function index()
    {
        $partnersCount = Partners::all()->count();
        return view('backend.partners.index');
    }
    public function dashboard_partners_home()
    {
    $all_partners_info=DB::table('partners')->get();
    return view('backend.partners.index', compact('all_partners_info'));
    }
    public function dashboard_partners_create()
    {
        return view('backend.partners.create');
    }

    public function dashboard_partners_store(Request $request)
    {
        $data=array();
        
    	$data['partner_name']=$request->partner_name;
    	$data['partner_description']=$request->partner_description;

        $image=$request->file('partner_image');
    	if($image)
      {
			$image_name=str_random(20);
			$ext=strtolower($image->getClientOriginalExtension());
			$image_full_name=$image_name.'.'.$ext;
			$upload_path='image/';
			$image_url=$upload_path.$image_full_name;
			$success=$image->move($upload_path,$image_full_name);
			if($success)
             {
				$data['partner_image']=$image_url;
			 }
           DB::table('partners')->insert($data);
           \Session::flash('flash_message','successfully saved.');
           
    	}
        return view('backend.partners.create');
    }
    public function deletepartners($partner_id){
    	DB::table('partners')
    	->where('partner_id',$partner_id)
    	->delete();
        Session::flash('flash_message','successfully deleted.');
    	return Redirect()->back();        
    }
    public function showesliders($partner_id){

    }
}
