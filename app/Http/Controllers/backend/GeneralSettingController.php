<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\file;
use App\Model\Generalsetting;
use DB;

class GeneralSettingController extends Controller
{

    public function general_setting(){
        $setting = GeneralSetting::first();
        return view('backend.general_setting',compact('setting'));
    }
    public function general_setting_store(Request $request)
    {           
        $this->validate($request,[
            'app_name' => 'required',
            'phone_number_1' => 'required',
            'phone_number_2' => 'nullable',
            'e_mail' => 'required',
        ]);
        $setting = GeneralSetting::first();
        if (!empty($setting)) {
            $setting->app_name = $request->app_name;
            $setting->phone_number_1 = $request->phone_number_1;
            $setting->phone_number_2 = $request->phone_number_2;
            $setting->e_mail = $request->e_mail;

            if($request->hasFile('app_logo')){
                $file_image = $request->file('app_logo');
                $file_name_image = time().rand(1,999).'.'.$file_image->getClientOriginalExtension();
                $file_image->move(base_path('public/logo'),$file_name_image);
                $setting->app_logo = $file_name_image;
            }
            $setting->update();
        }
        \Session::flash('flash_message','successfully Updated.');
        return redirect()->back()->with('success','Successfully Updated');

    }
}
