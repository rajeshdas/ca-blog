<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Redirect;
use Session;

class MessegeController extends Controller
{
    public function index()
    {
        $messegeCount = Messege::all()->count();
        return view('backend.messeage');
    }
    public function messege()
    {
    $all_messeges_info=DB::table('messeges')->get();
    return view('backend.message', compact('all_messeges_info'));
    }

    public function deletemessege($id){
    	DB::table('messeges')
    	->where('id',$id)
    	->delete();
        \Session::flash('flash_message','successfully deleted.');
    	return Redirect()->back();        
    }
}
