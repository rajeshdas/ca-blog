<?php

namespace App\Http\Controllers;
use App\Model\Sliders;
use Illuminate\Http\Request;
use App\Model\Partners;
use App\Model\GeneralSetting;
use DB;
use Redirect;

use Session;



class DashboardController extends Controller
{
    public function index()
    {
        $slidersCount = Sliders::all()->count();
        $partnersCount = Partners::all()->count();
        return view('backend.dashboard');
    }
    public function dashboard_home()
    {
    $setting = GeneralSetting::first();
    $all_sliders_info=DB::table('sliders')->get();
    return view('backend.home.home', compact('all_sliders_info','setting'));
    }
    public function dashboard_home_create()
    {
        return view('backend.home.create');
    }

    public function dashboard_home_store(Request $request)
    {
        $data=array();
        
    	$data['sliders_category']=$request->sliders_category;
    	$data['sliders_title']=$request->sliders_title;
        $data['sliders_name']=$request->sliders_name;
    	$data['sliders_tag']=$request->sliders_tag;

        $image=$request->file('sliders_image');
    	if($image)
      {
			$image_name=str_random(20);
			$ext=strtolower($image->getClientOriginalExtension());
			$image_full_name=$image_name.'.'.$ext;
			$upload_path='image/';
			$image_url=$upload_path.$image_full_name;
			$success=$image->move($upload_path,$image_full_name);
			if($success)
             {
				$data['sliders_image']=$image_url;
			 }
           DB::table('sliders')->insert($data);
           \Session::flash('flash_message','successfully saved.');
           
    	}
        return view('backend.home.create');
    }
    public function deletesliders($sliders_id){
    	DB::table('sliders')
    	->where('sliders_id',$sliders_id)
    	->delete();
        
        Session::flash('flash_message','successfully deleted.');
    	return Redirect()->back();        
    }
    public function showesliders($sliders_id){

    }
}
