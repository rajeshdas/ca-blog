<?php

namespace App\Http\Controllers;
use App\Model\Sliders;
use Illuminate\Http\Request;
use Session;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $slidersCount = DB::table('sliders')->get();
        $appCount = DB::table('general_setting')->get();
        $partnerCount = DB::table('partners')->get();
        return view('frontend.index', compact('slidersCount','appCount','partnerCount'));
    }
    public function contactcfo(){
        return view('frontend.services.contactcfo');
    }
    public function business_support_service(){
        return view('frontend.services.business_support_service');
    }
    public function consultancy(){
        return view('frontend.services.consultancy');
    }
    public function advisory_service(){
        return view('frontend.services.advisory_service');
    }
    public function audit_and_assurance(){
        return view('frontend.services.audit_and_assurance');
    }
    public function taxation(){
        return view('frontend.services.taxation');
    }
    public function messege()
    {
        return view('frontend.index');
    }
    public function messegepost(Request $request)
    {
        $data=array();
        
    	$data['name']=$request->name;
    	$data['phone']=$request->phone;
        $data['email']=$request->email;
    	$data['subject']=$request->subject;
        $data['messege']=$request->messege;

        $slidersCount = DB::table('sliders')->get();
        $appCount = DB::table('general_setting')->get();
        $partnerCount = DB::table('partners')->get();
        
        
        DB::table('messeges')->insert($data);
        \Session::flash('flash_message','successfully send.');
        return view('frontend.index',compact('slidersCount','appCount','partnerCount'));
    }
    

}

