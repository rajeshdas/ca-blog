<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use DB;

class MessegeController extends Controller
{
    public function index()
    {
        $appCount = DB::table('general_setting')->get();
        return view('frontend.index', compact('appCount'));
    }
    public function messegepost(Request $request)
    {
        $data=array();
        
    	$data['name']=$request->name;
    	$data['phone']=$request->phone;
        $data['email']=$request->email;
    	$data['subject']=$request->subject;
        $data['messege']=$request->messege;

        DB::table('messeges')->insert($data);
        \Session::flash('flash_message','successfully send.');
        return Redirect()->back();    
    }
}
