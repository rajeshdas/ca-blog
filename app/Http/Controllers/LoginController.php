<?php

namespace App\Http\Controllers;
use App\Model\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Check;
use DB;
use Session;
use Hash;

Session_start();
class LoginController extends Controller
{
    
    public function index()
    {
        $appCount = DB::table('general_setting')->get();
        return view('frontend.login', compact('appCount'));

    }
    public function dashboard(){
        return view('/backend.dashboard');
    }
    public function show_dashboard(Request $request){
        $request->validate([
            'email'=>'required|email',
            'password'=>'required'

        ]);
        $result=Admin::where('email',$request->email)->first();
        
        if($result){                       
            if(Hash::Check($request->password, $result->password));
            $request->Session()->put('LoginId', $result->id);
            return view('/dashboard');
            
        }
         else
        {
            
            return back();
        }
    }
    
}
