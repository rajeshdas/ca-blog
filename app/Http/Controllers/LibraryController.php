<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LibraryController extends Controller
{
    public function index()
    {
        $appCount = DB::table('general_setting')->get();
        return view('frontend.library', compact('appCount'));

    }
}
