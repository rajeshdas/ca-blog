<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PartnersController extends Controller
{
    public function index()
    {
        $slidersCount = DB::table('sliders')->get();
        $appCount = DB::table('general_setting')->get();
        return view('frontend.partners', compact('slidersCount','appCount'));

    }
}
