<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');
Route::get('/about', 'AboutController@index')->name('about');
Route::get('/contact', 'ContactController@index')->name('contact');
Route::get('/partners', 'PartnersController@index')->name('partners');
Route::get('/services', 'ServicesController@index')->name('services');
Route::get('/membership', 'MembershipController@index')->name('membership');
Route::get('/clients', 'ClientsController@index')->name('clients');
Route::get('/library', 'LibraryController@index')->name('library');
Route::get('/career', 'CareerController@index')->name('career');

Route::get('/contactcfo', 'HomeController@contactcfo')->name('contactcfo');
Route::get('/consultancy', 'HomeController@consultancy')->name('consultancy');
Route::get('/advisory_service', 'HomeController@advisory_service')->name('advisory_service');
Route::get('/business_support_service', 'HomeController@business_support_service')->name('business_support_service');
Route::get('/audit_and_assurance', 'HomeController@audit_and_assurance')->name('audit_and_assurance');
Route::get('/taxation', 'HomeController@taxation')->name('taxation');

Route::get('messege_request', 'HomeController@messege')->name('messege_request');
Route::post('messege_request', 'HomeController@messegepost')->name('messege_request.send');

  

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::group(['prefix' => 'admin'], function () {
//   Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
//   Route::post('/login', 'AdminAuth\LoginController@login');
//   Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

//   Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
//   Route::post('/register', 'AdminAuth\RegisterController@register');

//   Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
//   Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
//   Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
//   Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
// });
Auth::routes();

Route::group(['middleware'=>'auth'], function()
{
  Route::get('/admin_dashboard','LoginController@dashboard')->middleware('isLoggedIn');
  Route::get('/dashboard_home', 'DashboardController@dashboard_home')->name('dashboard_home');
  Route::get('/dashboard_home_create', 'DashboardController@dashboard_home_create')->name('dashboard_home_create');
  Route::post('/dashboard_home_store', 'DashboardController@dashboard_home_store')->name('post.store');
  Route::get('/deshboard_delete_sliders/{sliders_id}','DashboardController@deletesliders');
  Route::get('/deshboard_show_sliders/{sliders_id}','DashboardController@showesliders');
  Route::get('/dashboard','DashboardController@index')->name('dashboard');

  Route::get('/dashboard_about', 'backend\AboutController@dashboard_about')->name('dashboard_about');
  Route::get('/dashboard_about_create', 'backend\AboutController@about_create')->name('dashboard_about_create');
  Route::post('/dashboard_about_store', 'backend\AboutController@dashboard_about_store')->name('post.store');

  Route::get('/dashboard_general_setting', 'backend\GeneralSettingController@general_setting')->name('general_setting');
  Route::post('/dashboard_general_setting_Update', 'backend\GeneralSettingController@general_setting_store')->name('post.general_setting');

  Route::get('/dashboard_partners_home', 'backend\PartnersController@dashboard_partners_home')->name('dashboard_partners_home');
  Route::get('/dashboard_partners_create', 'backend\PartnersController@dashboard_partners_create')->name('dashboard_partners_create');
  Route::post('/dashboard_partners_create', 'backend\PartnersController@dashboard_partners_store')->name('dashboard_partners.post.store');
  Route::get('/deshboard_delete_partners/{partner_id}','backend\PartnersController@deletepartners');
  Route::get('/Clients_messege','backend\MessegeController@messege')->name('messege');
  Route::get('/deshboard_delete_messege/{id}','backend\MessegeController@deletemessege');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login', 'LoginController@index')->name('login');
Route::post('/dashboard','LoginController@show_dashboard')->name('admin_login');