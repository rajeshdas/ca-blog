@include('backend.layouts.css')
<body id="page-top">
    <div id="wrapper">
                
@include('backend.layouts.nav')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="card-title">Message List</h3>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 10px">Sl.</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Phone</th>
                                    <th class="text-center">Subject</th>
                                    <th class="text-center">E-mail</th>
                                    <th style="text-center">Message</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($all_messeges_info->count())
                                @foreach($all_messeges_info as $all_messeges)
                                    <tr>
                                        <td class="text-center">{{$all_messeges->id}}</td>
                                        <td class="text-center">{{$all_messeges->name}}</td>
                                        <td class="text-center">{{$all_messeges->phone}}</td>
                                        <td class="text-center">{{$all_messeges->subject}}</td>
                                        <td class="text-center">{{$all_messeges->email}}</td>
                                        <td class="text-center">{{$all_messeges->messege}}</td>
                                        <td class="d-flex">
                                            <!-- <a href="{{URL::to('/deshboard_show_messege/'.$all_messeges->id)}}" class="btn btn-sm btn-success mr-1"> <i class="fas fa-eye"></i> </a> -->
                                            <a href="{{URL::to('/deshboard_delete_messege/'.$all_messeges->id)}}" class="btn btn-sm btn-danger mr-1"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                 @endforeach
                                 @else   
                                    <tr>
                                        <td colspan="8">
                                            <h5 class="text-center">No posts found.</h5>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer d-flex justify-content-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            </div>
        </div>
    </div>
@extends('backend.layouts.footer')

@extends('backend.layouts.script')