@include('backend.layouts.css')
<body id="page-top">
<div id="wrapper">           
@include('backend.layouts.nav')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Create Post</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard_home') }}">Post list</a></li>
                    <li class="breadcrumb-item active">Create Post</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="card-title">Create Post</h3>
                            <a href="{{ route('dashboard_home') }}" class="btn btn-primary">Go Back to Post List</a>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-12 col-lg-8 offset-lg-2 col-md-8 offset-md-2">
                                <form action="{{ route('post.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body">
                                        @include('includes.errors')
                                        <div class="form-group">
                                            <label for="title">Heading Title</label>
                                            <input type="name" name="sliders_title" value="{{ old('title') }}" class="form-control" placeholder="Enter title">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Heading Tag</label>
                                            <input type="name" name="sliders_tag" value="{{ old('tag') }}" class="form-control" placeholder="Enter Tag">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Heading Name</label>
                                            <input type="name" name="sliders_name" value="{{ old('name') }}" class="form-control" placeholder="Enter Tag">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Sliders Category</label>
                                            <input type="name" name="sliders_category" value="{{ old('category') }}" class="form-control" placeholder="Enter Tag">
                                        </div>

                                        <div class="form-group">
                                            <label for="image">Image</label>
                                            <div class="custom-file">
                                                <input type="file" name="sliders_image" class="custom-file-input" id="image" required>
                                                <label class="custom-file-label" for="image">Choose file</label>
                                            </div>
                                        </div>                                       
                                    </div>

                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>
@extends('backend.layouts.footer')

@extends('backend.layouts.script')