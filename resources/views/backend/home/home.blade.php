@include('backend.layouts.css')
<body id="page-top">
    <div id="wrapper">
                
@include('backend.layouts.nav')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="card-title">Heading Slider With Text</h3>
                            <a href="dashboard_home_create" class="btn btn-primary">Create New</a>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 10px">Sl.</th>
                                    <th class="text-center">Image</th>
                                    <th class="text-center">Category</th>
                                    <th class="text-center">Title</th>
                                    <th class="text-center">Tags</th>
                                    <th style="width: 40px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($all_sliders_info->count())
                                @foreach($all_sliders_info as $v_sliders)
                                    <tr>
                                        <td class="text-center">{{$v_sliders->sliders_id}}</td>
                                        <td class="text-center"><img src="{{URL::to($v_sliders->sliders_image)}}" style="width:120px; height:40px;overflow:hidden"></td>
                                        <td class="text-center">{{$v_sliders->sliders_category}}</td>
                                        <td class="text-center">{{$v_sliders->sliders_title}}</td>
                                        <td class="text-center">{{$v_sliders->sliders_tag}}</td>
                                        <td class="d-flex">
                                            <a href="{{URL::to('/deshboard_show_sliders/'.$v_sliders->sliders_id)}}" class="btn btn-sm btn-success mr-1"> <i class="fas fa-eye"></i> </a>
                                            <a href="#" class="btn btn-sm btn-primary mr-1"> <i class="fas fa-edit"></i> </a>
                                            <a href="{{URL::to('/deshboard_delete_sliders/'.$v_sliders->sliders_id)}}" class="btn btn-sm btn-danger mr-1"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                 @endforeach
                                 @else   
                                    <tr>
                                        <td colspan="6">
                                            <h5 class="text-center">No posts found.</h5>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer d-flex justify-content-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            </div>
        </div>
    </div>
@extends('backend.layouts.footer')

@extends('backend.layouts.script')