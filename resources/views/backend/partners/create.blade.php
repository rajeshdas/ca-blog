@include('backend.layouts.css')
<body id="page-top">
<div id="wrapper">           
@include('backend.layouts.nav')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Create Post</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard_home') }}">Post list</a></li>
                    <li class="breadcrumb-item active">Create Post</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="card-title">Create Post</h3>
                            <a href="{{ route('dashboard_partners_home') }}" class="btn btn-primary">Go Back to Post List</a>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-12 col-lg-8 offset-lg-2 col-md-8 offset-md-2">
                                <form action="{{ route('dashboard_partners.post.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body">
                                        @include('includes.errors')
                                        <div class="form-group">
                                            <label for="title"><b>Name and Designation</b><span class="text-danger">*</span></label>
                                            <input type="name" name="partner_name" value="{{ old('title') }}" class="form-control" placeholder="Enter Name and Designation">
                                        </div>
                                        <div class="form-group">
                                            <label for="title"><b>Description</b><span class="text-danger">*</span></label>
                                            <input type="name" name="partner_description" value="{{ old('tag') }}" class="form-control" placeholder="Enter Partners Description">
                                        </div>
                                        <div class="form-group">
                                            <label for="image">Image<span class="text-danger">*</span></label>
                                            <div class="custom-file">
                                                <input type="file" name="partner_image" class="custom-file-input" id="image" required>
                                                <label class="custom-file-label" for="image">Choose file</label>
                                            </div>
                                        </div>                                       
                                    </div>

                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>
@extends('backend.layouts.footer')

@extends('backend.layouts.script')