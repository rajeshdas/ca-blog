@include('backend.layouts.css')

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">
    @include('backend.layouts.nav')
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
        <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                            <h3 class="card-title">General Setting</h3>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-12 col-lg-8 offset-lg-2 col-md-8 offset-md-2">
                                <form action="{{ route('post.general_setting') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body">
                                        @include('includes.errors')
                                        <div class="form-group">
                                            <label for="app_name" class="col-form-label pt-0"><b>App Name</b><span class="text-danger">*</span></label>
                                            <input type="name" name="app_name" value="" class="form-control{{ $errors->has('app_name') ? ' is-invalid' : '' }}" placeholder="App Name">
                                             @if ($errors->has('app_name'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('app_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="title" class="col-form-label pt-0"><b>Phone Number 1</b><span class="text-danger">*</span></label>
                                            <input type="name" name="phone_number_1" value="{{ $setting->phone_number_1 }}" class="form-control {{ $errors->has('phone_number_1') ? ' is-invalid' : '' }}" placeholder="Phone Number 1">
                                             @if ($errors->has('phone_number_1'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('phone_number_1') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="title" class="col-form-label pt-0"><b>Phone Number 2</b><span class="text-danger">*</span></label>
                                            <input type="name" name="phone_number_2" value="{{ $setting->phone_number_2 }}" class="form-control {{ $errors->has('phone_number_2') ? ' is-invalid' : '' }}" placeholder="Phone Number 2">
                                             @if ($errors->has('phone_number_2'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('phone_number_2') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="title" class="col-form-label pt-0"><b>E-mail</b><span class="text-danger">*</span></label>
                                            <input type="name" name="e_mail" value="{{ $setting->e_mail }}" class="form-control" placeholder="E-mail">
                                            @if ($errors->has('e_mail'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('e_mail') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="title" class="col-form-label pt-0"><b>App Logo</b><span class="text-danger">*</span></label>
                                            <div class="custom-file">
                                                <input type="file" name="app_logo" class="custom-file-input" value="{{ $setting->app_logo }}" id="image" require>
                                                <label class="custom-file-label" for="image">Choose file</label>
                                            </div>
                                        </div>  

                                        <div class="form-group">
                                             <button type="submit" class="btn btn-lg btn-primary" style="float:right";>Save & Update</button>
                                        </div>                                     
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @extends('backend.layouts.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->




    @include('backend.layouts.script')