@include('backend.layouts.css')

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">
    @include('backend.layouts.nav')
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">


                        <!--This is COntent-->


                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @extends('backend.layouts.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->




    @include('backend.layouts.script')