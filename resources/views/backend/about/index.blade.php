@include('backend.layouts.css')
<body id="page-top">
    <div id="wrapper">
                
@include('backend.layouts.nav')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h3 class="card-title">Heading Slider With Text</h3>
                            <a href="dashboard_about_create" class="btn btn-primary">Create New</a>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 10px">Sl.</th>
                                    <th class="text-center">Image</th>
                                    <th class="text-center">Category</th>
                                    <th class="text-center">Title</th>
                                    <th class="text-center">Tags</th>
                                    <th style="width: 40px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer d-flex justify-content-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            </div>
        </div>
    </div>
@extends('backend.layouts.footer')

@extends('backend.layouts.script')