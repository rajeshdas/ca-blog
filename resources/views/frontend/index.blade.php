@include('frontend.layouts.header')
@include('frontend.layouts.nav')
@include('frontend.layouts.css')
@section('content')

<div class="banner-carousel banner-carousel-1 mb-0">
@if($slidersCount->count())
@foreach($slidersCount as $slidersitem)
  <div class="banner-carousel-item" style="background-image:url({{ asset($slidersitem->sliders_image) }});">
    <div class="slider-content">
        <div class="container h-100">
          <div class="row align-items-center h-100">
              <div class="col-md-12 text-center">
                <h5 class="slide-sub-title" data-animation-in="slideInLeft" style="color:white; font-style:bold;">{{$slidersitem->sliders_title}}</h2>
                <h2 class="slide-title" data-animation-in="slideInRight" style="color:white;">{{$slidersitem->sliders_tag}}</h3>                
              </div>
          </div>
        </div>
    </div>
  </div>
  @endforeach
  @endif
</div>


<section class="call-to-action-box no-padding">
  <div class="container">
    <div class="action-style-box">
        <div class="row align-items-center">
          <div class="col-md-8 text-center text-md-left">
              <div class="call-to-action-text">
                <h3 class="action-title">We understand your needs on Chartered Accout</h3>
              </div>
          </div><!-- Col end -->
          <div class="col-md-4 text-center text-md-right mt-3 mt-md-0">
              <div class="call-to-action-btn">
                <a class="btn btn-dark" href="#">Request Quote</a>
              </div>
          </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- Action style box -->
  </div><!-- Container end -->
</section><!-- Action end -->

<section id="ts-features" class="ts-features">
  <div class="container">
    <div class="row">
        <div class="col-lg-4" style="background-color:#48BDC5;padding-top:20px">
          <div class="ts-intro">
              <h3 align="center">Our Vision & Mission</h3>
              <p>We are rethoric question ran over her cheek When she reached the first hills of the Italic Mountains,
                she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village
                and the subline of her own road, the Line Lane.</p>
          </div>
        </div>
        <div class="col-lg-4" style="background-color:#5BC4CB;padding-top:20px">
          <div class="ts-intro">
              <h3 align="center">OUR PRIDE</h3>
              <p>We pride ourselves on being at the cutting edge of our professional service and continuously looking at ways to enhance our quality of service.
                 Our innovation approach is the audit effectiveness and efficiency. We are a member firm of CH International, UK</p>
          </div>
        </div>
        <div class="col-lg-4" style="background-color:#6FEBF8;padding-top:20px">
          <div class="ts-intro">
              <h3 align="center">SERVICES</h3>
              <p>T.HUSSAIN provides a wide range of high quality services to its clients of private and public sectors in Bangladesh. 
            It also renders services to international development agencies and expatriate consultants those are associated with various projects in Bangladesh.</p>
          </div>
        </div>
    </div>  
  </div>
</section>
<!-- Feature are end -->

<section id="facts" class="facts-area dark-bg">
  <div class="container">
    <div class="facts-wrapper">
        <div class="row">
          <div class="col-md-3 col-sm-6 ts-facts">
              <div class="ts-facts-img">
                <img loading="lazy" src="images/icon-image/fact1.png" alt="facts-img">
              </div>
              <div class="ts-facts-content">
                <h2 class="ts-facts-num"><span class="counterUp" data-count="10">0</span></h2>
                <h3 class="ts-facts-title">Total Projects</h3>
              </div>
          </div><!-- Col end -->

          <div class="col-md-3 col-sm-6 ts-facts mt-5 mt-sm-0">
              <div class="ts-facts-img">
                <img loading="lazy" src="images/icon-image/fact2.png" alt="facts-img">
              </div>
              <div class="ts-facts-content">
                <h2 class="ts-facts-num"><span class="counterUp" data-count="5">0</span></h2>
                <h3 class="ts-facts-title">Staff Members</h3>
              </div>
          </div><!-- Col end -->

          <div class="col-md-3 col-sm-6 ts-facts mt-5 mt-md-0">
              <div class="ts-facts-img">
                <img loading="lazy" src="images/icon-image/fact3.png" alt="facts-img">
              </div>
              <div class="ts-facts-content">
                <h2 class="ts-facts-num"><span class="counterUp" data-count="6000">0</span></h2>
                <h3 class="ts-facts-title">Hours of Work</h3>
              </div>
          </div><!-- Col end -->

          <div class="col-md-3 col-sm-6 ts-facts mt-5 mt-md-0">
              <div class="ts-facts-img">
                <img loading="lazy" src="images/icon-image/fact4.png" alt="facts-img">
              </div>
              <div class="ts-facts-content">
                <h2 class="ts-facts-num"><span class="counterUp" data-count="6">0</span></h2>
                <h3 class="ts-facts-title">Countries Experience</h3>
              </div>
          </div>

        </div>
    </div>
  </div>
</section>

<section id="ts-service-area" class="ts-service-area pb-0">
  <div class="container">
    <div class="row text-center">
        <div class="col-12">
          <h2 class="section-title">We Are Specialists In</h2>
          <h3 class="section-sub-title">What We Do</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon1.png" alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">CONTRACT CFO</a></h3>
                <p class="text">Are you a small to medium sized business that require the expertise of a CFO, but do not have the space or the resources?...
                <a href="{{ route('contactcfo') }}"><b>Read More</b></a></p>
                  
                </div>
          </div><!-- Service 1 end -->

          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon2.png" alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">CONSULTANCY</a></h3>
                <p class="text">Are you a small to medium sized business that require the expertise of a CFO, but do not have the space or the of the year fkhasdk kjhadsk resources?...
                <a href="{{ route('consultancy') }}"><b>Read More</b></a></p>
              </div>
          </div><!-- Service 2 end -->

          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon3.png"  alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">ADVISORY SERVICES</a></h3>
                <p class="text">Are you a small to medium king of bangladesh sized business that require the expertise of a CFO, but do not have the space or the resources?...
                <a href="{{ route('advisory_service') }}"><b>Read More</b></a></p>
              </div>
          </div><!-- Service 3 end -->

        </div><!-- Col end -->

        <div class="col-lg-4 text-center">
          <img loading="lazy" class="img-fluid" src="images/45003.png" alt="service-avater-image" style="opacity:.3;">
        </div><!-- Col end -->

        <div class="col-lg-4 mt-5 mt-lg-0 mb-4 mb-lg-0">
          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon4.png" alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">BUSINESS SUPPORT SERVICES</a></h3>
                <p class="text">Are you a small to medium sized business that require the expertise of a CFO, but do not have the space or the resources?...
                <a href="{{ route('business_support_service') }}"><b>Read More</b></a></p>
              </div>
          </div><!-- Service 4 end -->

          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon5.png" alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">AUDIT & ASSURANCE</a></h3>
                <p class="text">Are you a small to medium sized business that require the expertise of a CFO, but do not have the space or the resources?...
                <a href="{{ route('audit_and_assurance') }}"><b>Read More</b></a></p>
              </div>
          </div><!-- Service 5 end -->

          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon6.png" alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">TAXATION</a></h3>
                <p class="text">Are you a small to medium sized business that require the expertise of a CFO, but do not have the space or the resources?...
                <a href="{{ route('taxation') }}"><b>Read More</b></a></p>
              </div>
          </div><!-- Service 6 end -->
        </div><!-- Col end -->
    </div>

  </div>
</section>

<section class="team-section section">
    <div class="container">
        <div class="section-title text-center">
            <h3>Our
                <span>Partners</span>
            </h3>
        </div>
        <div class="row">
          @if($partnerCount->count())
            @foreach($partnerCount as $partneritem)		 
              <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="team-member">
                      <img style="height:300px;width:300px; align:center;"  src="{{asset($partneritem->partner_image)}}" alt="doctor" class="img-responsive">
                      <div class="contents text-center">
                          <h4>{{$partneritem->partner_name}}</h4>
                          <p>{{$partneritem->partner_description}}<br/>
                          <a href="#" class="btn btn-primary">read more</a>
                      </div>
                  </div>
              </div>
              @endforeach
          @endif
        </div>
    </div>
</section>

<section class="appoinment-section section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="accordion-section">
    <div class="section-title">
        <h3>About US</h3>
    </div>
                    <div class="panel-body" style="text-align:justify;">
                    Contrary to popular belief, Lorem Ipsum is not simply random text. 
                    It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. 
                    Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, 
                    looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, 
                    and going through the cites of the word in classical literature, discovered the undoubtable source.
                     Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" 
                     (The Extremes of Good and Evil) by Cicero, written in 45 BC. 
                     This book is a treatise on the theory of ethics, very popular during the Renaissance.
                      The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                       The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.
                        Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum"
                         by Cicero are also reproduced in their exact original form,
                     accompanied by English versions from the 1914 translation by H. Rackham.
                    </div>
          </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="contact-area">
    <div class="section-title">
        <h3>Request</h3>
    </div>
    <form autocomplete="off" name="contact_form" class="default-form contact-form" action="{{route('messege_request.send')}}" method="post">
    @csrf 
    <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                    <input type="text" name="name" placeholder="Name" required="" class="form-control form-control-lg">
                </div>
                <div class="form-group">
                    <input type="email" name="email" placeholder="Email" required="" class="form-control form-control-lg">
                </div>
               
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                    <input type="text" name="phone" placeholder="Phone" required="" class="form-control form-control-lg">
                </div>
                <div class="form-group">
                    <input type="text" name="subject" placeholder="Subject" required="" class="form-control form-control-lg">
                </div>
               
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <textarea class="form-control form-control-message" name="messege" placeholder="Your messege" rows="10" required=""></textarea>
                </div>
                <div class="form-group text-center">
                <button class="btn btn-primary solid blank" type="submit">Submit Now</button>
                </div>
            </div>
        </div>
    </form>
</div>                        
            </div>
        </div>                    
    </div>
</section>
><!-- Content end -->

<section class="subscribe no-padding">
  <div class="container">
    <div class="row">
        <div class="col-md-4">
          <div class="subscribe-call-to-acton">
              <h3>May I Help You?</h3>
              <h4>+8801832406258</h4>
          </div>
        </div><!-- Col end -->

        <div class="col-md-8">
          <div class="ts-newsletter row align-items-center">
              <div class="col-md-5 newsletter-introtext">
                <h4 class="text-white mb-0">Newsletter Sign-up</h4>
                <p class="text-white">Latest updates and news</p>
              </div>

              <div class="col-md-7 newsletter-form">
                <form action="#" method="post">
                    <div class="form-group">
                      <label for="newsletter-email" class="content-hidden">Newsletter Email</label>
                      <input type="email" name="email" id="newsletter-email" class="form-control form-control-lg" placeholder="Your your email and hit enter" autocomplete="off">
                    </div>
                </form>
              </div>
          </div><!-- Newsletter end -->
        </div><!-- Col end -->

    </div><!-- Content row end -->
  </div>
  <!--/ Container end -->
</section>

<!--/ subscribe end -->
@extends('frontend.layouts.footer')

@extends('frontend.layouts.script')
  </div><!-- Body inner end -->
