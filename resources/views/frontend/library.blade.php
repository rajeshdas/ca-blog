@include('frontend.layouts.header')
@include('frontend.layouts.nav')
@include('frontend.layouts.css')

<div class="banner-carousel banner-carousel-1 mb-0">
  <div class="banner-carousel-item" style="background-image:url({{ asset('') }}images/library.jpg); height:300px;">
    <div class="slider-content">
        <div class="container h-100">
          <div class="row align-items-center h-100">
              <div class="col-md-12 text-center">
                <h2 class="slide-title" data-animation-in="slideInLeft" style=" font-size:70px;font-family:Source Sans Pro, sans-serif;font-style:bold;">LIBRARY</h2>
              </div>
          </div>
        </div>
    </div>
  </div>
</div>
<br>
<div class="container">
  <h2 align="center">
    Library
  </h2>
  <hr>
  <div class="row">
      <div class="col-md-12">
            <h2>Company Profile</h2>
            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years,
               sometimes by accident, sometimes on purpose (injected humour and the like).</p>
            <p><a href="#" style="color:#4B9905">Read more</a>
        </div>
  </div> 
  <hr>     

</div>

@extends('frontend.layouts.footer')

@extends('frontend.layouts.script')