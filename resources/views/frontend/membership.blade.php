@include('frontend.layouts.header')
@include('frontend.layouts.nav')
@include('frontend.layouts.css')

<div class="banner-carousel banner-carousel-1 mb-0">
  <div class="banner-carousel-item" style="background-image:url({{ asset('') }}images/membership.jpg); height:300px;">
    <div class="slider-content">
        <div class="container h-100">
          <div class="row align-items-center h-100">
              <div class="col-md-12 text-center">
                <h2 class="slide-title" data-animation-in="slideInLeft" style=" font-size:70px;font-family:Source Sans Pro, sans-serif;font-style:bold;">MEMBERSHIP</h2>
              </div>
          </div>
        </div>
    </div>
  </div>
</div>
<br>
<div class="container">
  <h2 align="center">
    Membership List
  </h2>
  <hr>
  <div class="row">
      <div class="col-md-8">
            <p>Basis</p>
            <p>+8801111111111</p>
            <p>basis@gmail.com</p>
            <a href="">Read more</a>
        </div>
        <div class="col-md-4">
        <img style="height:170px;width:250px;" src="{{asset('images/basis.jpg')}}" alt="doctor" class="img-responsive">
        </div>
  </div> 
  <hr>     
  <div class="row">
      <div class="col-md-8">
            <p>City corporation</p>
            <p>8801111111111</p>
            <p>citycorporation@gmail.com</p>
            <a href="">Read more</a>
        </div>
        <div class="col-md-4">
        <img style="height:170px;width:250px;" src="{{asset('images/city-corporation.png')}}" alt="doctor" class="img-responsive">
        </div>
  </div> 
  <hr>     
  <div class="row">
      <div class="col-md-8">
            <p>Bangladesh Bank</p>
            <p>8801111111111</p>
            <p>bdbank@gmail.com</p>
            <a href="">Read more</a>
        </div>
        <div class="col-md-4">
        <img style="height:170px;width:250px;" src="{{asset('images/bd-bank.png')}}" alt="doctor" class="img-responsive">
        </div>
        
  </div>
  <hr>     
</div>

@extends('frontend.layouts.footer')

@extends('frontend.layouts.script')