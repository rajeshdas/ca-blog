@include('frontend.layouts.header')
@include('frontend.layouts.nav')
@include('frontend.layouts.css')

<div class="banner-carousel banner-carousel-1 mb-0">
  <div class="banner-carousel-item" style="background-image:url({{ asset('') }}images/services.jpg); height:300px">
    <div class="slider-content">
        <div class="container h-100">
          <div class="row align-items-center h-100">
              <div class="col-md-12 text-center">
                <h2 class="slide-title" data-animation-in="slideInLeft">Service</h2>
              </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="container">
  <br>
<div class="row text-center">
        <div class="col-12">
          <h3 class="section-sub-title">We Are Specialists In</h3>
        </div>
    </div>
<div class="row">
        <div class="col-lg-4">
          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon1.png" alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">CONTRACT CFO</a></h3>
                <p class="text">Are you a small to medium sized business that require the expertise of a CFO, but do not have the space or the resources?...
                <a href="{{ route('contactcfo') }}"><b>Read More</b></a></p>
                  
                </div>
          </div><!-- Service 1 end -->

          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon2.png" alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">CONSULTANCY</a></h3>
                <p class="text">Are you a small to medium sized business that require the expertise of a CFO, but do not have the space or the of the year fkhasdk kjhadsk resources?...
                <a href="{{ route('consultancy') }}"><b>Read More</b></a></p>
              </div>
          </div><!-- Service 2 end -->

          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon3.png"  alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">ADVISORY SERVICES</a></h3>
                <p class="text">Are you a small to medium king of bangladesh sized business that require the expertise of a CFO, but do not have the space or the resources?...
                <a href="{{ route('advisory_service') }}"><b>Read More</b></a></p>
              </div>
          </div><!-- Service 3 end -->

        </div><!-- Col end -->

        <div class="col-lg-4 text-center">
          <img loading="lazy" class="img-fluid" src="images/45003.png" alt="service-avater-image" style="opacity:.3;">
        </div><!-- Col end -->

        <div class="col-lg-4 mt-5 mt-lg-0 mb-4 mb-lg-0">
          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon4.png" alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">BUSINESS SUPPORT SERVICES</a></h3>
                <p class="text">Are you a small to medium sized business that require the expertise of a CFO, but do not have the space or the resources?...
                <a href="{{ route('business_support_service') }}"><b>Read More</b></a></p>
              </div>
          </div><!-- Service 4 end -->

          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon5.png" alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">AUDIT & ASSURANCE</a></h3>
                <p class="text">Are you a small to medium sized business that require the expertise of a CFO, but do not have the space or the resources?...
                <a href="{{ route('audit_and_assurance') }}"><b>Read More</b></a></p>
              </div>
          </div><!-- Service 5 end -->

          <div class="ts-service-box d-flex">
              <div class="ts-service-box-img">
                <img loading="lazy" src="images/icon-image/service-icon6.png" alt="service-icon">
              </div>
              <div class="ts-service-box-info">
                <h3 class="service-box-title"><a href="#">TAXATION</a></h3>
                <p class="text">Are you a small to medium sized business that require the expertise of a CFO, but do not have the space or the resources?...
                <a href="{{ route('taxation') }}"><b>Read More</b></a></p>
              </div>
          </div><!-- Service 6 end -->
        </div><!-- Col end -->
    </div>
</div>

@extends('frontend.layouts.footer')

@extends('frontend.layouts.script')