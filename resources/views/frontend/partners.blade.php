@include('frontend.layouts.header')
@include('frontend.layouts.nav')
@include('frontend.layouts.css')

<div class="banner-carousel banner-carousel-1 mb-0">
  <div class="banner-carousel-item" style="background-image:url({{ asset('') }}images/partner.png); height:300px;">
    <div class="slider-content">
        <div class="container h-100">
          <div class="row align-items-center h-100">
              <div class="col-md-12 text-center">
                <h2 class="slide-title" data-animation-in="slideInLeft" style="color:#4DA002; font-size:50px;font-family:Source Sans Pro, sans-serif;">Partner</h2>
              </div>
          </div>
        </div>
    </div>
  </div>
</div>
<br>
<div class="container">
  <h2 align="center">
    Our Partners
  </h2>
  <hr>
  <div class="row">
      <div class="col-md-8">
            <p>Jersey Gallery</p>
            <p>01736622658</p>
            <p>jerseygallerybd"gmail.com</p>
            <a href="">Read more</a>
        </div>
        <div class="col-md-4">
        <img style="height:170px;width:250px;" src="{{asset('images/team/team1.jpg')}}" alt="doctor" class="img-responsive">
        </div>
        
  </div> 
  <hr>     
  <div class="row">
      <div class="col-md-8">
            <p>Fashion Gallery</p>
            <p>01736622658</p>
            <p>jerseygallerybd"gmail.com</p>
            <a href="">Read more</a>
        </div>
        <div class="col-md-4">
        <img style="height:170px;width:250px;" src="{{asset('images/team/team3.jpg')}}" alt="doctor" class="img-responsive">
        </div>
        
  </div> 
  <hr>     
  <div class="row">
      <div class="col-md-8">
            <p>News Gallery</p>
            <p>01736622658</p>
            <p>jerseygallerybd"gmail.com</p>
            <a href="">Read more</a>
        </div>
        <div class="col-md-4">
        <img style="height:170px;width:250px;" src="{{asset('images/team/team2.jpg')}}" alt="doctor" class="img-responsive">
        </div>
        
  </div> 
  <hr>     
  <div class="row">
      <div class="col-md-8">
            <p>Jersey Gallery</p>
            <p>01736622658</p>
            <p>jerseygallerybd"gmail.com</p>
            <a href="">Read more</a>
        </div>
        <div class="col-md-4">
        <img style="height:170px;width:250px;" src="{{asset('images/team/team4.jpg')}}" alt="doctor" class="img-responsive">
        </div>
        
  </div>    
</div>

@extends('frontend.layouts.footer')

@extends('frontend.layouts.script')