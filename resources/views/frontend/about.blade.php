@include('frontend.layouts.header')
@include('frontend.layouts.nav')
@include('frontend.layouts.css')

<div class="banner-carousel banner-carousel-1 mb-0">
@if($slidersCount->count())
@foreach($slidersCount as $slidersitem)
  <div class="banner-carousel-item" style="background-image:url({{ asset($slidersitem->sliders_image) }});">
    <div class="slider-content">
        <div class="container h-100">
          <div class="row align-items-center h-100">
              <div class="col-md-12 text-center">
                <h5 class="slide-sub-title" data-animation-in="slideInLeft" style="color:white; font-style:bold;">{{$slidersitem->sliders_title}}</h2>
                <h2 class="slide-title" data-animation-in="slideInRight" style="color:white;">{{$slidersitem->sliders_tag}}</h3>                
              </div>
          </div>
        </div>
    </div>
  </div>
  @endforeach
  @endif
</div>
<div class="container">
<section>
<p>
Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
</p>
</section>
</div>




@extends('frontend.layouts.footer')

@extends('frontend.layouts.script')