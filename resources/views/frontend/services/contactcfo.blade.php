@include('frontend.layouts.nav')
@include('frontend.layouts.css')

<div class="banner-carousel banner-carousel-1 mb-0">
  <div class="banner-carousel-item1" style="background-image:url({{ asset('') }}/images/3.jpg); height:300px; opacity:5 ;">
    <div class="slider-content">
        <div class="container h-100">
          <div class="row align-items-center h-100">
              <div class="col-md-12 text-center">
                <h1 class="slide-title" data-animation-in="slideInLeft" style="color:white;">Contact Us</h1>
              </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="container">
T. Hussain & Co. offers clients a broad range of business support services which includes corporate secretarial services. We work closely with clients to deliver timely, high quality services.

Registrar of Joint Stock Companies (RJSC )

  o   Formation of companies

  o   Annual return for AGM

  o   Annual return for changes

  o   Memorandum and Articles of Association

  o   Memorandum and Articles of Association


BSEC/DSE/CSE related services
 o   AGM/EGM/Board meeting holding &  minutes

 o   Submission of quarterly/half yearly reports

 o   PSI reports submission

 o   Annual report preparation

 o   BSEC/DSE/CSE query answers

Capital raising/working capital management

 o   IPO/RPO management

 o   Foreign financing –IFC, ECA/IDA/WEG/ supplier credit/OBU/LTFF

 o   LC management –BTB/EDF/Sight/Deferred/UPAS

 o   Debt-equity approval from Bangladesh Bank/BOI

 o   EPZ company type A,B and C’s fund management

Investment & Expatriate Services

 o   BIDA registration

 o   E-visa and work permit related services

 o   Security clearance

 o   VISA extension for expatriate and their dependents

 o   Investment & feasibility analysis

Software Development & deployment

 o   Prepare Requirement Analysis

 o   Guiding Software Developer

 o   Testing Raw Software

 o   Automation and implementation with live data

 o   User training & user manual preparation

Trade & Commerce

 o   Trade License

 o   Trade Marks

 o   Copy right

 o   TIN Certificate

 o   VAT Registration



Corporate and individual tax planning

General and tax compliance advisory

Deferred tax computation and application

Tax compliance and tax return preparation



Tax assessments, appeal, tribunal & ADR

Transfer Pricing

Assistance in handling queries and requests for information from tax authorities

Tax Holiday approval
</div>




@extends('frontend.layouts.footer')

@extends('frontend.layouts.script')