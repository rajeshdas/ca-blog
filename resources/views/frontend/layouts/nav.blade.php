
<body>
<div class="body-inner">
<!-- Header start -->
<header id="header" class="header-one">
  <div class="site-navigation" style="background-color:#5BC4CB">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
              <nav class="navbar navbar-expand-lg navbar-dark p-0">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div id="navbar-collapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav mr-auto">
                      <li class="nav-item"><a class="nav-link" href="{{ route('index') }}">Home</a></li>
                      <li class="nav-item"><a class="nav-link" href="{{ route('about') }}">About</a></li>
                      <li class="nav-item"><a class="nav-link" href="{{ route('contact') }}">Contact</a></li>
                      <li class="nav-item"><a class="nav-link" href="{{ route('partners') }}">Partners</a></li>
                      <li class="nav-item"><a class="nav-link" href="{{ route('services') }}">Services</a></li>
                      <li class="nav-item"><a class="nav-link" href="{{ route('membership') }}">Membership</a></li>
                      <li class="nav-item"><a class="nav-link" href="{{ route('clients') }}">Clients</a></li>
                      <li class="nav-item"><a class="nav-link" href="{{ route('library') }}">Library</a></li>
                      <li class="nav-item"><a class="nav-link" href="{{ route('career') }}">Career</a></li>
                      <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                      
                    </ul>
                </div>
              </nav>
          </div>
          <!--/ Col end -->
        </div>
        <!--/ Row end -->

        <div class="nav-search">
          <span id="search"><i class="fa fa-search"></i></span>
        </div><!-- Search end -->

        <div class="search-block" style="display: none;">
          <label for="search-field" class="w-100 mb-0">
            <input type="text" class="form-control" id="search-field" placeholder="Type what you want and enter">
          </label>
          <span class="search-close">&times;</span>
        </div><!-- Site search end -->
    </div>
    <!--/ Container end -->

  </div>
  <!--/ Navigation end -->
</header>
