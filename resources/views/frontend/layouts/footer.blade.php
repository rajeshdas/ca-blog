<footer id="footer" class="footer bg-overlay">
    <div class="footer-main">
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-lg-4 col-md-6 footer-widget footer-about">
            <img loading="lazy" width="80px" class="footer-logo" src="images/attachment_14940716.png" alt="logo">
            <p>T.HUSSAIN & Co. was formed in 1987, with a mission to continually add value by helping clients succeed.</p>
            <ul class="location-link">
            <p><i class="fa fa-map-marker"></i>
                3 indira road Farmgate , Dhaka-1205</p>
            <p><i class="fa fa-envelope mr-0" aria-hidden="true"></i>
                 rajeshdasbd24@gmail.com</p>
                <p><i class="fa fa-phone" aria-hidden="true"></i>
                +8801832406258, +88011816653535, +88</p>
            </ul>
            <div class="footer-social">
              <ul>
                <li><a href="" aria-label="Facebook"><i
                      class="fab fa-facebook-f"></i></a></li>
                <li><a href="" aria-label="Twitter"><i class="fab fa-twitter"></i></a>
                </li>
                <li><a href="" aria-label="Instagram"><i
                      class="fab fa-instagram"></i></a></li>
                <li><a href="" aria-label="Github"><i class="fab fa-github"></i></a></li>
              </ul>
            </div><!-- Footer social end -->
          </div><!-- Col end -->

          <div class="col-lg-4 col-md-6 footer-widget mt-5 mt-md-0">
            <h3 class="widget-title">TOP & POPULAR LINK</h3>
            <ul class="list-arrow">
              <li><a href="{{ route('about') }}">About</a></li>
              <li><a href="{{ route('partners') }}">Partners</a></li>
              <li><a href="{{ route('services') }}">Services</a></li>
              <li><a href="{{ route('membership') }}">Membership</a></li>
              <li><a href="{{ route('clients') }}">Clients</a></li>
              <li><a href="{{ route('library') }}">Libraries</a></li>
            </ul>
          </div><!-- Col end -->

          <div class="col-lg-3 col-md-6 mt-5 mt-lg-0 footer-widget">
            <h3 class="widget-title">Services</h3>
            <ul class="list-arrow">
              <li><a href="service-single.html">ADVISORY SERVICES</a></li>
              <li><a href="service-single.html">AUDIT & ASSURANCE</a></li>
              <li><a href="service-single.html">BUSINESS SUPPORT SERVICES</a></li>
              <li><a href="service-single.html">CONSULTANCY</a></li>
              <li><a href="service-single.html">Contract CFO</a></li>
              <li><a href="service-single.html">TAXATION</a></li>
            </ul>
          </div><!-- Col end -->
        </div><!-- Row end -->
      </div><!-- Container end -->
    </div><!-- Footer main end -->

    <div class="copyright">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6">
            <div class="copyright-info text-center text-md-left">
              <span> &copy;Copyright 2018.<script>
                  
                </script> All Rights Reserved By T.HUSSAIN & CO. Develop By :  <a href="#">RajeshDas</a></span>
            </div>
          </div>

          <div class="col-md-6">
            <div class="footer-menu text-center text-md-right">
              <ul class="list-unstyled">
                <li><a href="{{route('index')}}">Home</a></li>
                <li><a href="{{route('about')}}">About</a></li>
                <li><a href="{{route('contact')}}">Contact</a></li>
              </ul>
            </div>
          </div>
        </div><!-- Row end -->

        <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top position-fixed">
          <button class="btn btn-primary" title="Back to Top">
            <i class="fa fa-angle-double-up"></i>
          </button>
        </div>

      </div><!-- Container end -->
    </div><!-- Copyright end -->
  </footer><!-- Footer end -->