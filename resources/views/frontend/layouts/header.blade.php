<div class="bg-white" style="padding-top:30px;">
    <div class="container">
      <div class="logo-area">
      @if($appCount->count())
                @foreach($appCount as $appitem)
          <div class="row align-items-center">
            <div class="logo col-lg-3 text-center text-lg-left mb-3 mb-md-5 mb-lg-0">
          
                <a class="d-block" href="{{ route('index') }}">
                  <img loading="lazy" src="{{ asset('/logo/'.$appitem->app_logo) }}" alt="logo" style="height:80px; wedth:80px;">
                </a>
                
            </div><!-- logo end -->
  
            <div class="col-lg-9 header-right">
                <ul class="top-info-box">
                <li>
                    <div class="info-box">
                      <div class="info-box-content">
                          <h2 style="color:#008000;">Rajesh Das</h2>
                          <p style="color:#4c30FF">Laravel Developer</p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="info-box">
                      <div class="info-box-content">
                          <h3 style="color:#222222;"><i class="fa fa-phone"><b>&nbsp;&nbsp;Call Us</b></i></h3>
                          <p><a href="tel:(+88) {{$appitem->phone_number_1 }}">{{$appitem->phone_number_1 }},&nbsp;{{$appitem->phone_number_2 }}</a></p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="info-box">
                      <div class="info-box-content">
                      <h3 style="color:#222222;"><i class="fa fa-envelope">&nbsp;&nbsp;Email Us</i></h3>
                          <p>{{$appitem->e_mail }}</p>
                      </div>
                    </div>
                  </li>
                </ul><!-- Ul end -->
            </div><!-- header right end -->
          </div><!-- logo area end -->
  
      </div><!-- Row end -->
      @endforeach
      @endif
    </div><!-- Container end -->
  </div>