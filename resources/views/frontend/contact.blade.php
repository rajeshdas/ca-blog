@include('frontend.layouts.header')
@include('frontend.layouts.nav')
@include('frontend.layouts.css')

<div class="banner-carousel banner-carousel-1 mb-0">
  <div class="banner-carousel-item1" style="background-image:url({{ asset('') }}images/3.jpg); height:300px; opacity:5 ;">
    <div class="slider-content">
        <div class="container h-100">
          <div class="row align-items-center h-100">
              <div class="col-md-12 text-center">
                <h1 class="slide-title" data-animation-in="slideInLeft" style="color:white;">Contact Us</h1>
              </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="container">
<form autocomplete="off" name="contact_form" class="default-form contact-form" action="{{route('messege_request.send')}}" method="post">
        <div class="row" style="margin-top:20px;">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                    <input type="text" name="name" placeholder="Name" required="" class="form-control form-control-lg">
                </div>
                <div class="form-group">
                    <input type="email" name="email" placeholder="Email" required="" class="form-control form-control-lg">
                </div>
               
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                    <input type="text" name="phone" placeholder="Phone" required="" class="form-control form-control-lg">
                </div>
                <div class="form-group">
                    <input type="text" name="subject" placeholder="Subject" required="" class="form-control form-control-lg">
                </div>
               
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <textarea class="form-control form-control-message" name="message" id="message" placeholder="Your messege" rows="10" required=""></textarea>
                </div>
                <div class="form-group text-center">
                <button class="btn btn-primary solid blank" type="submit">Submit Now</button>
                </div>
            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-md-12">
        <ul class="location-link">
            <p>Rajesh Das was formed in 2020, with a mission to continually add value by helping clients succeed.</p>
            <p><i class="fa fa-map-marker"></i>
                Century Dell (3rd Floor) 3 indira road
            (Farmgate), Dhaka-1205</p>
            <p><i class="fa fa-envelope mr-0" aria-hidden="true"></i>
                 rajeshdasbd24@gmail.com</p>
                <p><i class="fa fa-phone" aria-hidden="true"></i>
                +8801832406258, +8801816653535</p>
            </ul>
        </div>

    </div>

</div>




@extends('frontend.layouts.footer')

@extends('frontend.layouts.script')